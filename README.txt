CONTENTS OF THIS FILE
---------------------

* Warning Drush
* Overview
* What does it do?
* Set up Google anylytics
* Supported social networks
* Hyves
* Design Decisions
* Requirements
* Keywords

Warning Drush
-------------
This will not install via drush (because of dependency to
google analytics).

Overview
---------

A Drupal 7 version of Social media user detection the Wordpress plugin.
http://wordpress.org/extend/plugins/social-network-user-detection/
This is a complete rewrite of the Wordpress module for Drupal 7.

What does it do ?
-----------------

If a visitor is logged in to one of the supported Social networks(see below)
then this will be recorded to your Google Analytics account that
you have set up.

This way you have an perfect overview of what your visitors are using at
the time they are browsing your website.

Set up Google anylytics
-----------------------

How to set up Google Analytics ? Custom segments, custom reports
This will create a nice overview !
Please visit this link :
http://adaptpartners.com/internet-marketing-tools/
social-media-user-detection

Just click on the links and then they will be automatically
added to your GA Screen.

Supported social networks
-------------------------

* Facebook
* Twitter
* Google +
* Gmail/Normal Google Account
* hyves

Hyves
------

If you are using HYVES.
add "hcrpc_relay.html" to your WEB ROOT !
Report for hyves to be used in GA by me:
https://www.google.com/analytics/web/permalink?uid=cXObeqcRQTCP_tD56rEzcw

Requirements
------------

Google Analytics module (must be set up aswell)

Keywords
--------

Social Media
Social media user detection
Social networks
